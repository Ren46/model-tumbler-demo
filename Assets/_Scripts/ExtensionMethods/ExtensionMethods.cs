﻿/* Extension methods created by Brendan Polley 2021
 * Handy, reusable extension methods for MSC2006H and beyond
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    //Covert world postions of a target to anchored UI positions
    public static void WorldToAnchoredPosition(this RectTransform rect, Transform target)
    {
        Canvas canvas = rect.GetComponentInParent<Canvas>();
        RectTransform canvasRectTransform = canvas.GetComponent<RectTransform>();
        Vector3 targetPos = target.position;

        Vector2 viewportPos = Camera.main.WorldToViewportPoint(targetPos);
        Vector2 canvasPos = new Vector2(
            ((viewportPos.x * canvasRectTransform.sizeDelta.x) - (canvasRectTransform.sizeDelta.x * 0.5f)),
            ((viewportPos.y * canvasRectTransform.sizeDelta.y) - (canvasRectTransform.sizeDelta.y * 0.5f))
            );

        rect.anchoredPosition = canvasPos;
    }

    //
    public static bool IsOccluded(this Transform trans)
    {
        RaycastHit hit;
        Vector3 camPos = Camera.main.transform.position;
        Vector3 direction = trans.position - camPos;
        if(Physics.Raycast(camPos, direction, out hit, Mathf.Infinity))
        {
            if (hit.transform != trans)
                return true;
        }
        return false;
    }

    
}